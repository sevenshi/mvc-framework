package mvc.core;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MvcConfigParseListener implements ServletContextListener {
	
	public static final String actionMapInServletContext = "actionMapInMvc";
	

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		Map<String, ActionModel> map = ConfigParser.scanPackage();
		context.setAttribute(actionMapInServletContext, map);
	}

}
