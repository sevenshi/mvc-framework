package example.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.core.ActionForm;
import mvc.core.annotation.Controller;
import example.formbean.LoginForm;


public class ExampleAction {
    

	@Controller(path="abc",formType="example.formbean.LoginForm" )
	public String success(HttpServletRequest request,
			HttpServletResponse response, ActionForm form) {
		LoginForm f = (LoginForm)form;
		System.out.println("Example Action username:" + f.getUsername());
		System.out.println("Example Action password:" + f.getPassword());
		if("seven".equals(f.getUsername())){
		    return "success";
		}else {
            return "failure";
        }
	}
}
